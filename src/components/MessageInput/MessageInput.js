import React, { useState } from 'react';
import './MessageInput.css';

const MessageInput = (props) => {
  const [text, setText] = useState('');

  const handleAddMessage = (event) => {
    event.preventDefault();
    if (text !== '') {
      props.addMessage(text);
      setText('');
    }

  }
  const handleEditMessage = (event) => {
    event.preventDefault();
    props.showEdit();
  }
  const handleKeyPress = (event) => {

    if (event.key === 'Enter' && event.shiftKey) {
      handleAddMessage(event);
    }

  }
  const handleKeyDown = (event) => {
    if (event.key === 'ArrowUp' && text === '') {
      handleEditMessage(event);
    }

  }
  return (
    <div className="Message-input">
      <textarea
        onChange={ev => setText(ev.target.value)}
        onKeyPress={ev => handleKeyPress(ev)}
        onKeyDown={ev => handleKeyDown(ev)}
        value={text}
        placeholder="Type your message and push button 'Send' or press 'Shift+Enter'"
        type="text" />
      <div class="Input-button">
        <button onClick={ev => handleAddMessage(ev)} >Send</button>
      </div>
    </div>
  );
}


export default MessageInput;
