import React from 'react';
import './Message.css';
import editTool from './editTool.svg';
import trashTool from './trashTool.svg';
import heart from './heart.svg';
import emptyHeart from './emptyHeart.svg'

const Message = (props) => {
    const handleDelete = (event) => {
        props.deleteMessage(props.message.id);
    }
    const handleEdit = (event) => {
        props.showEdit(props.message.text, props.message.id);
    }
    const handleIsLike = (event) => {
        props.liked(props.userId, props.message.id, props.isLike);
    }
    return (
        <div className={props.message.userId === props.userId ? "Message Message-user" : "Message"}>
            <span>
                {new Date(props.message.createdAt).toLocaleString().split(",")[1]}
            </span>
            <div className="Message-box">
                {props.message.userId !== props.userId &&
                    <img src={props.message.avatar} className="User-avatar" />}
                <p>
                    {props.message.text}

                    {props.message.userId === props.userId &&
                        <div className="Message-tools">
                            <label size="small" as="a" onClick={(ev) => handleEdit(ev)} >
                                <img src={editTool} className="Icon" />
                            </label>
                            <label size="small" as="a" onClick={(ev) => handleDelete(ev)}>
                                <img src={trashTool} className="Icon" />
                            </label>
                        </div>
                    }
                </p>
                {props.message.userId !== props.userId &&
                    <label onClick={(ev) => handleIsLike(ev)} className="Like-icon">
                        {props.isLike ?
                            <img src={heart} className="Like-icon" /> :
                            <img src={emptyHeart} className="Like-icon" />}
                    </label>
                }
            </div>
        </div >
    );
}

export default Message;
