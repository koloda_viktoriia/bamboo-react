import React from 'react';
import './ChatHeader.css';
import kasa from './kasa.svg';
import origami from './origami.svg';

const ChatHeader = (props) => {

  const messagesCount = props.data.length;

  const usersCounter = () => {
    const usersId = [];
    props.data.map(message => {
      if (usersId.indexOf(message.userId) === -1) {
        usersId.push(message.userId)
      }
    });
    return (usersId.length);
  };
  const convertTime = (lastDate) => {
    const now = new Date().getTime();
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (new Date(now).toLocaleDateString() === new Date(lastDate).toLocaleDateString()) {
      const time = new Date(lastDate).toLocaleString().split(",")[1].split(":");
      return (time[0] + ":" + time[1]);
    } else if (new Date(now).getFullYear() === new Date(lastDate).getFullYear()) {
      const indexMonth = new Date(now).getMonth();
      return (months[indexMonth] + " " + new Date(now).getDate());
    }
    else {
      const time = new Date(lastDate).toLocaleDateString();
      return (time);
    }
  }
  const lastMessage = props.data[props.data.length - 1];
  return (
    <div className="Chat-header">
      <span className="Chat-users Header-item">
        <img src={kasa} className="Chat-icon" />
        {usersCounter()}
      </span>
      <span className="Chat-messages Header-item">
        <img src={origami} className="Chat-icon" />
        {messagesCount}
      </span>
      <span className="Chat-title ">Panda's team chat</span>
      <span className="Chat-last Header-item"> Last message at {convertTime(lastMessage.createdAt)}</span>
    </div>
  );
}

export default ChatHeader;
