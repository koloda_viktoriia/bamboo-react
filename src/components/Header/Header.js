import React from 'react';
import './Header.css';

import logo from './logo.svg';

const Header=()=> {
  return (
    <header>
        <img src={logo} className="Header-logo" alt="logo"/>
        <span>Bamboo</span>
    </header>
    );
}

export default Header;