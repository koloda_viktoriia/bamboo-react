import React, { useRef, useEffect } from 'react';
import './MessageList.css';
import Message from '../Message/Message';


const MessageList = (props) => {
  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView();
  }

  useEffect(scrollToBottom, [props.data]);
  const dates = props.data.map(item => new Date(item.createdAt).toLocaleDateString());
  let currentDate = props.data[0].createdAt;

  const checkDate = (date) => {
    const messageDate = new Date(date).toLocaleDateString();
    if (new Date(currentDate).toLocaleDateString() !== messageDate) {
      currentDate = date;
      return (<span className="Message-date">{convertDate(currentDate)}</span>);
    }
  }

  const convertDate = (date) => {
    const currentDay = new Date(date).toLocaleDateString();
    const today = new Date().toLocaleDateString();
    const yesterday = new Date(new Date().getDate() - 1).toLocaleDateString();
    if (currentDay === today) {
      return ("Today");
    } else if (currentDay === yesterday) {
      return ("Yesterday");
    } else {
      return new Date(date).toLocaleDateString();
    }
  }

  console.log(props.data);
  return (
    <div className="Message-list">
      <span className="Message-date">{convertDate(currentDate)}</span>
      {props.data.map(message => {
        const like = props.likes
          .find(like => {
            return ((like.userId === props.userId) & (like.id === message.id))
          });
        const isLike = (like !== undefined);

        return (
          <>
            {checkDate(message.createdAt)}
            <Message
              isLike={isLike}
              message={message}
              deleteMessage={props.deleteMessage}
              showEdit={props.showEdit}
              liked={props.liked}
              userId={props.userId}
            />
          </>);
      })}
      <div ref={messagesEndRef} />
    </div>
  );

}

export default MessageList;
