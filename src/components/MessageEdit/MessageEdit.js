import React, { useState } from 'react';
import './MessageEdit.css';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as actions from '../../redux/actions';

const MessageEdit = () => {
  var { text, id } = useSelector(state => state.editMessage, shallowEqual);
  const [textValue, setTextValue] = useState(text);
  const dispatch = useDispatch();

  const onCancel = () => {
    dispatch(actions.hideEdit());
  }

  const onEdit = () => {
    dispatch(actions.editMessage(textValue, id));
    dispatch(actions.hideEdit());
  }

  return (

    <>
      <div class="Modal-background">
        <div id="Modal">
          <div className="Modal-header">
            <span>Edit message</span>
          </div>
          <div className="Modal-edit">
            <textarea
              onChange={(ev) => setTextValue(ev.target.value)}
              value={textValue}
              type="text" />
            <div class="Modal-buttons">
              <button onClick={onCancel} className="cancelButton">Cancel</button>
              <button onClick={onEdit} className="editButton">Edit</button>
            </div>
          </div>
        </div>
      </div>

    </>
    // {/* <button className="btn btn-secondary" onClick={()=>onCancel()}>Cancel</button>
    //            <button className="btn btn-primary" onClick={()=>onSave()}>Save</button> */}

    // </div>


  );
}



export default MessageEdit;
