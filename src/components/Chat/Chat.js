import React, { useState, useEffect, useCallback } from 'react';
import Header from '../Header/Header';
import ChatHeader from '../ChatHeader/ChatHeader';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import MessageEdit from '../MessageEdit/MessageEdit';
import Footer from '../Footer/Footer';
import './Chat.css';
import logo from './logo.svg';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as actions from '../../redux/actions';


const Chat = () => {
  const messages = useSelector(state => state.messages, shallowEqual);
  const likes = useSelector(state => state.likes, shallowEqual);
  const userProfile = useSelector(state => state.userProfile, shallowEqual);
  const isEdit = useSelector(state => state.isEdit, shallowEqual);
  const dispatch = useDispatch();
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setisLoading(false), 500);
  });


  messages.sort(
    (a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });
  const showEdit = (text, id) => {
    console.log(text, id)
    if (id === undefined) {
      const userMessages = messages
        .filter(message => message.userId === userProfile.userId);
      if (userMessages.length === 0) return;
      const message = userMessages.reduce((prev, current) => (prev.createdAt > current.createdAt) ? prev : current);
      text = message.text;
      id = message.id;
    }
    dispatch(actions.showEdit(text, id));
  }

  const addMessage = (text) => {
    console.log(text);
    dispatch(actions.addMessage(text));
  }

  const deleteMessage = (id) => {
    dispatch(actions.deleteMessage(id));
  }

  const liked = (userId, id, isLike) => {
    console.log(isLike);
    dispatch(actions.liked(userId, id, isLike));
  }

  return (
    isLoading
      ?
      <div class="Spinner">
        <img src={logo} className="Spinner-logo" alt="logo" />
      </div>
      :
      <>
        <Header />
        <div className="Chat">
          <ChatHeader
            data={messages} />
          <MessageList data={messages}
            deleteMessage={deleteMessage}
            likes={likes}
            showEdit={showEdit}
            userId={userProfile.userId}
            liked={liked} />
          <MessageInput addMessage={addMessage} showEdit={showEdit} />
        </div>
        {isEdit ? <MessageEdit /> : null}
        <Footer />

      </>
  );

}

export default Chat;
