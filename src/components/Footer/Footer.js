import React from 'react';
import './Footer.css';


const Footer = () => {
  return (
    <footer>
      <span>&copy; Victoria Koloda, 2020</span>
    </footer>
  );
}

export default Footer;