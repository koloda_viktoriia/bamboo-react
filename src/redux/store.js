import { createStore } from 'redux';
import data from '../data.json';
import userProfile from '../user.json';
import { v4 as uuidv4 } from 'uuid';

const INITIAL_STATE = {
    messages: data,
    likes: [],
    userProfile,
    isEdit: false,
    editMessage: undefined
};

const chatStore = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ADD_MESSAGE':
            const { text } = action.payload;
            const message = {
                id: uuidv4(),
                userId: userProfile.userId,
                avatar: userProfile.avatar,
                user: userProfile.user,
                text: text,
                createdAt: new Date().getTime(),
                updatedAt: ""
            }
            return { ...state, messages: [...state.messages, message] };
        case 'UPDATE_MESSAGE':
            {
                const { text, id } = action.payload;
                const editedMessage = (message) => (
                    {
                        ...message,
                        text,
                        updatedAt: new Date().getTime()
                    });
                const updatedMessages = state.messages.map(message =>
                    message.id === id ? editedMessage(message) : message);
                return { ...state, messages: updatedMessages };
            }
        case 'DELETE_MESSAGE':
            {
                const { id } = action.payload;
                const newMessages = state.messages.filter(message => message.id !== id);
                return { ...state, messages: newMessages };
            }
        case 'LIKED':
            {
                const { userId, id, isLike } = action.payload;
                if (isLike) {
                    const newLikes = state.likes
                        .filter(like =>
                            (like.id !== id) && (like.userId !== userId));
                    return {
                        ...state,
                        likes: newLikes
                    };
                } else {
                    const like =
                    {
                        userId: userId,
                        id: id
                    };
                    const list = [...state.likes, like];
                    return { ...state, likes: list };
                }
            }
        case 'SHOW_EDIT': {
            const editMessage = action.payload;
            return { ...state, isEdit: true, editMessage: editMessage };
        }
        case 'HIDE_EDIT':
            return { ...state, isEdit: false };
        default:
            return state;
    }
}

const store = createStore(chatStore);

export default store;