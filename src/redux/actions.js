

export const addMessage = (text) => {
    return { type: 'ADD_MESSAGE', payload: { text } };
}
export const editMessage = (text, id) => {
    return { type: 'UPDATE_MESSAGE', payload: { text, id } };

}
export const deleteMessage = (id) => {
    return { type: 'DELETE_MESSAGE', payload: { id } };

}
export const liked = (userId, id, isLike) => {
    return { type: 'LIKED', payload: { userId, id, isLike } };
}
export const showEdit = (text, id) => {
    console.log('act');
    console.log(text, id);
    return { type: 'SHOW_EDIT', payload: { text, id } };

}
export const hideEdit = () => {
    return { type: 'HIDE_EDIT', payload: {} }
}