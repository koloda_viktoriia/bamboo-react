import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './components/Chat/Chat.js';
import store  from './redux/store';
import {Provider } from 'react-redux';

ReactDOM.render(
 <React.StrictMode>
       <Provider store={store}>
    <Chat/>
    </Provider >
  </React.StrictMode>,
  document.getElementById('root')
);



// serviceWorker.unregister();
